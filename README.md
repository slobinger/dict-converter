# DictConverter class
DictConverter is a Python3 class to convert dict keys based on a mapping

## integration in other projects
the project directory is already structured as python package.
If one wants to package it for pip, debian, or something else it is possible to
simply use it as git submodule in the destribution specific packaging project.

The same is possible to use the class in your own project if you do not want to
package anything.

```bash
git submodule add $this_project_ref dict_converter
```

In place of dict_converter also any other name can be used. In python
package names **must not** contain dashes. Package names have to be
**all lowercase** and **words are connected by unerscore**.

## use

```python
import dict_converter

# lets say e need a dict for ldap, in which username is defined by uid.
model_ldap_mapping = {
    'username': 'uid'
}
model_dict_2_ldap_dict = DictConverter(mapping = model_ldap_mapping)

# Lets say we have a model that provides the following dict

model_dict = {
    'username': 'dude'
    'givenName': 'Bob'
    'surName': 'Dude'
}

ldap_dict = model_dict_2_ldap_dict.convertForward(model_dict)

print(ldap_dict)
"""
{'uid':'dude','givenName':'Bob','surName':'Dude'}
"""

# We can also convert backwards

print(model_dict_2_ldap_dict.convertBackwards(ldap_dict))
"""
{'username':'dude','givenName':'Bob','surName':'Dude'}
"""