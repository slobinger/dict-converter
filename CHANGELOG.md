# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

The semantic of the versioning scheme is similar to
[Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html)

## [1.0.1] 2019-10-28

### Changed

- The unittest has changed so that it fails if the dict_converter uses `is`
  instead of `==` in the wrong place. See bug description below for details.

### Fixed

- In some cases e.g. when dict to convert comes from json the converter as faild.
  This was because the comparisation of the dict keys was made with `is` not
  `==` operator. This problem is now fixed by using `==` instead of `is`.

## [1.0.0] 2019-10-28

### Added

- this CHANGELOG.md to provide an easy to read overview of the
  current project status
- README.md to describe the behavior of the Dictconverter class
- DictConverter class with ability to initialize with mapping and the
  methods convertForward and convertBackward
- .gitignore to ignore all environment specific stuff like caches, virtuelenvs
  and so on
